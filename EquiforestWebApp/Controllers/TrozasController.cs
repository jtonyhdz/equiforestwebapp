﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EquiforestWebApp.Data;
using EquiforestWebApp.Models;

namespace EquiforestWebApp.Controllers
{
    public class TrozasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TrozasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Trozas
        public async Task<IActionResult> Index(int? pageNumber, DateTime fecini, DateTime fecfin)
        {
            int pageSize = 10;
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");
            //return View(await _context.Trozas.ToListAsync());
            return View(await PaginatedList<Trozas>.CreateAsync(_context.Trozas.Where(f => f.Viaje.FechaIngreso>=_fecini && f.Viaje.FechaIngreso <= _fecfin).Include(troza => troza.Viaje).AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        public async Task<IActionResult> Resumen()
        {
            //var result = from s in _context.Trozas join v in _context.Viajes on s.ViajesId equals v.Id
            //             select new ResumenCosecha { Circunferencia = s.Circunferencia, Clasificacion = s.Clasificacion, Largo = s.Largo, Volumen = ((Math.Sqrt((Double)s.Circunferencia))/(4*Math.PI))*s.Largo/10000 };
            // return View(await result.AsNoTracking().ToListAsync());
            return View(await _context.Trozas.AsNoTracking().ToListAsync());
        }

        // GET: Trozas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trozas = await _context.Trozas
                .SingleOrDefaultAsync(m => m.Id == id);
            if (trozas == null)
            {
                return NotFound();
            }

            return View(trozas);
        }

        // GET: Trozas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Trozas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdViaje,Clasificacion,Circunferencia,Largo")] Trozas trozas)
        {
            if (ModelState.IsValid)
            {
                _context.Add(trozas);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(trozas);
        }

        // GET: Trozas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trozas = await _context.Trozas.SingleOrDefaultAsync(m => m.Id == id);
            if (trozas == null)
            {
                return NotFound();
            }
            return View(trozas);
        }

        // POST: Trozas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdViaje,Clasificacion,Circunferencia,Largo")] Trozas trozas)
        {
            if (id != trozas.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(trozas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TrozasExists(trozas.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(trozas);
        }

        // GET: Trozas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var trozas = await _context.Trozas
                .SingleOrDefaultAsync(m => m.Id == id);
            if (trozas == null)
            {
                return NotFound();
            }

            return View(trozas);
        }

        // POST: Trozas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var trozas = await _context.Trozas.SingleOrDefaultAsync(m => m.Id == id);
            _context.Trozas.Remove(trozas);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TrozasExists(int id)
        {
            return _context.Trozas.Any(e => e.Id == id);
        }
    }
}
