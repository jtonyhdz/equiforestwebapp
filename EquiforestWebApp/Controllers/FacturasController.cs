﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EquiforestWebApp.Data;
using EquiforestWebApp.Models;

namespace EquiforestWebApp.Controllers
{
    public class FacturasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FacturasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Facturas
        public async Task<IActionResult> Index(int? pageNumber, DateTime fecini, DateTime fecfin)
        {
            int pageSize = 10;
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");

            //return View(await _context.Facturas.ToListAsync());
            return View(await PaginatedList<Facturas>.CreateAsync(_context.Facturas.Include(t => t.Contenedores).Where(f => f.Fecha >= _fecini && f.Fecha <= _fecfin).AsNoTracking(), pageNumber ?? 1, pageSize));
        }

        public async Task<IActionResult> PackingList(string Facturano, string packinglistid, DateTime fecini, DateTime fecfin, int origen)
        {
            var _fecini = fecini.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : fecini.Date;
            var _fecfin = fecfin.Year < 1900 ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)) : fecfin.Date;
            ViewData["FecIni"] = _fecini.ToString("yyyy-MM-dd");
            ViewData["FecFin"] = _fecfin.ToString("yyyy-MM-dd");

            List<Estratos> _estratos = new List<Estratos>();
            _estratos.Add(new Estratos { Id = 0, MinValue = 12, MaxValue = 13, Name = "12" });
            _estratos.Add(new Estratos { Id = 1, MinValue = 13, MaxValue = 14, Name = "13" });
            _estratos.Add(new Estratos { Id = 2, MinValue = 14, MaxValue = 15, Name = "14" });
            _estratos.Add(new Estratos { Id = 3, MinValue = 15, MaxValue = 16, Name = "15" });
            _estratos.Add(new Estratos { Id = 4, MinValue = 16, MaxValue = 17, Name = "16" });
            _estratos.Add(new Estratos { Id = 5, MinValue = 17, MaxValue = 18, Name = "17" });
            _estratos.Add(new Estratos { Id = 6, MinValue = 18, MaxValue = 19, Name = "18" });
            _estratos.Add(new Estratos { Id = 7, MinValue = 19, MaxValue = 20, Name = "19" });
            _estratos.Add(new Estratos { Id = 8, MinValue = 20, MaxValue = 21, Name = "20" });
            _estratos.Add(new Estratos { Id = 9, MinValue = 21, MaxValue = 22, Name = "21" });
            _estratos.Add(new Estratos { Id = 10, MinValue = 22, MaxValue = 23, Name = "22" });
            _estratos.Add(new Estratos { Id = 11, MinValue = 23, MaxValue = 24, Name = "23" });
            _estratos.Add(new Estratos { Id = 12, MinValue = 24, MaxValue = 25, Name = "24" });
            _estratos.Add(new Estratos { Id = 13, MinValue = 25, MaxValue = 99999, Name = "25" });
            ViewData["Estratos"] = _estratos;
            ViewData["ShowFilter"] = true;
            List<PackingListResumen> model = null;

            switch (origen)
            {
                //Comes from Factura
                case 1:
                    ViewData["ShowFilter"] = false;
                    var result1 = 
                        from s in _context.DetalleContenedores
                                 join v in _context.PackingList on s.PackingListId equals v.PackingListId
                                 join c in _context.Contenedores on v.PackingListId equals c.PackingListId
                                 join f in _context.Facturas on c.FacturaId equals f.FacturaId
                                 where f.FacturaId == Facturano
                                 select new PackingListResumen
                                 {
                                     Cliente = f.Cliente,
                                     Fecha = f.Fecha,
                                     FacturaId = f.FacturaId,
                                     Cabezal = c.Cabezal,
                                     Especie = f.Especie,
                                     Calidad = v.Clase,
                                     ContenedorId = c.ContenedorId,
                                     Finca = f.Finca,
                                     Chofer = c.Chofer,
                                     Sello = c.Sello,
                                     Grupo = "Trozas con corteza",
                                     PackingListId = v.PackingListId,
                                     Diametro = Convert.ToSingle(s.Circunferencia / Math.PI),
                                     Largo = s.Longitud,
                                     Volumen = MathF.Round((float)(((s.Circunferencia * s.Circunferencia) / (4 * Math.PI)) * s.Longitud / 10000), 2)
                                 };
                    model = await result1.OrderBy(o => o.Fecha).ThenBy(o => o.FacturaId).AsNoTracking().ToListAsync();
                    break;
                
                //Comes from PackingList - Global
                case 2:
                    var result2 = from s in _context.DetalleContenedores
                                 join v in _context.PackingList on s.PackingListId equals v.PackingListId
                                 where v.Fecha >= _fecini && v.Fecha <= _fecfin && v.ContenedorId == null
                                 select new PackingListResumen
                                 {
                                     Cliente = "",
                                     Fecha = v.Fecha,
                                     FacturaId = "",
                                     Cabezal = "",
                                     Especie = "Teca",
                                     Calidad = v.Clase,
                                     ContenedorId = "",
                                     Finca = v.Ubicacion,
                                     Chofer = "",
                                     Sello = "",
                                     Grupo = "Trozas con corteza",
                                     PackingListId = v.PackingListId,
                                     Diametro = Convert.ToSingle(s.Circunferencia / Math.PI),
                                     Largo = s.Longitud,
                                     Volumen = MathF.Round((float)(((s.Circunferencia * s.Circunferencia) / (4 * Math.PI)) * s.Longitud / 10000), 2)
                                 };
                    model = await result2.OrderBy(o => o.Fecha).ThenBy(o => o.FacturaId).AsNoTracking().ToListAsync();
                    break;
                
                //Comes from PackingList - Print
                case 3:
                    ViewData["ShowFilter"] = false;
                    var result3 = from s in _context.DetalleContenedores
                                  join v in _context.PackingList on s.PackingListId equals v.PackingListId
                                  where v.PackingListId == packinglistid
                                  select new PackingListResumen
                                  {
                                      Cliente = "",
                                      Fecha = v.Fecha,
                                      FacturaId = "",
                                      Cabezal = "",
                                      Especie = "Teca",
                                      Calidad = v.Clase,
                                      ContenedorId = "",
                                      Finca = v.Ubicacion,
                                      Chofer = "",
                                      Sello = "",
                                      Grupo = "Trozas con corteza",
                                      PackingListId = v.PackingListId,
                                      Diametro = Convert.ToSingle(s.Circunferencia / Math.PI),
                                      Largo = s.Longitud,
                                      Volumen = MathF.Round((float)(((s.Circunferencia * s.Circunferencia) / (4 * Math.PI)) * s.Longitud / 10000), 2)
                                  };
                    model = await result3.OrderBy(o => o.Fecha).ThenBy(o => o.FacturaId).AsNoTracking().ToListAsync();
                    break;
            }

            /*if (Facturano == null )
            {
                var result = from s in _context.DetalleContenedores
                             join v in _context.PackingList on s.PackingListId equals v.PackingListId
                             where v.Fecha >= _fecini && v.Fecha <= _fecfin && v.ContenedorId == null
                             select new PackingListResumen
                             {
                                 Cliente = "",
                                 Fecha = v.Fecha,
                                 FacturaId = "",
                                 Cabezal = "",
                                 Especie = "",
                                 Calidad = v.Clase,
                                 ContenedorId = "",
                                 Finca = v.Ubicacion,
                                 Chofer = "",
                                 Sello = "",
                                 Grupo = "Trozas con corteza",
                                 PackingListId = v.PackingListId,
                                 Diametro = Convert.ToSingle(s.Circunferencia / Math.PI),
                                 Largo = s.Longitud,
                                 Volumen = MathF.Round((float)(((s.Circunferencia * s.Circunferencia) / (4 * Math.PI)) * s.Longitud / 10000), 2)
                             };
                model = await result.OrderBy(o => o.Fecha).ThenBy(o => o.FacturaId).AsNoTracking().ToListAsync();
            }
            else
            {
                ViewData["ShowFilter"] = false;
                var result = from s in _context.DetalleContenedores
                             join v in _context.PackingList on s.PackingListId equals v.PackingListId
                             join c in _context.Contenedores on v.PackingListId equals c.PackingListId
                             join f in _context.Facturas on c.FacturaId equals f.FacturaId
                             where f.FacturaId == Facturano
                             select new PackingListResumen
                             {
                                 Cliente = f.Cliente,
                                 Fecha = f.Fecha,
                                 FacturaId = f.FacturaId,
                                 Cabezal = c.Cabezal,
                                 Especie = f.Especie,
                                 Calidad = v.Clase,
                                 ContenedorId = c.ContenedorId,
                                 Finca = f.Finca,
                                 Chofer = c.Chofer,
                                 Sello = c.Sello,
                                 Grupo = "Trozas con corteza",
                                 PackingListId = v.PackingListId,
                                 Diametro = Convert.ToSingle(s.Circunferencia / Math.PI),
                                 Largo = s.Longitud,
                                 Volumen = MathF.Round((float)(((s.Circunferencia * s.Circunferencia) / (4 * Math.PI)) * s.Longitud / 10000), 2)
                             };
                model = await result.OrderBy(o => o.Fecha).ThenBy(o => o.FacturaId).AsNoTracking().ToListAsync();
            }*/

            var packingList = model.OrderBy(o => o.PackingListId).GroupBy(g => g.PackingListId);
                //_context.PackingList.Where(f => f.FacturaId == Facturano).AsNoTracking().ToList();
            ViewData["PackingList"] = packingList;
            ViewData["Factura"] = Facturano;
            return View(model);
        }

        // GET: Facturas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facturas = await _context.Facturas
                .SingleOrDefaultAsync(m => m.Id == id);
            if (facturas == null)
            {
                return NotFound();
            }

            return View(facturas);
        }

        // GET: Facturas/Create
        public IActionResult Create()
        {
            ViewData["Fincas"] = GetFincas();
            return View();
        }

        // POST: Facturas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FacturaId,Fecha,Cliente,Especie,Finca,Incoterm,Destino,BillOfLading")] Facturas facturas)
        {
            if (ModelState.IsValid)
            {
                _context.Add(facturas);
                //Create the prices 
                List<Precios> precios = new List<Precios>()
                {
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "40-44"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "45-49"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "50-54"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "55-59"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "60-64"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "65-69"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "70-74"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "75-79"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "80-84"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "85-89"},
                    new Precios {FacturaId=facturas.FacturaId, Estrato = "90->>"}
                };
                _context.Precios.AddRange(precios);
                    //.Add(precios);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["Fincas"] = GetFincas();
            return View(facturas);
        }

        // GET: Facturas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facturas = await _context.Facturas.SingleOrDefaultAsync(m => m.Id == id);
            if (facturas == null)
            {
                return NotFound();
            }
            return View(facturas);
        }

        // POST: Facturas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FacturaNo,Fecha,Cliente,Especie,Finca,Incoterm,Destino,BillOfLading")] Facturas facturas)
        {
            if (id != facturas.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(facturas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FacturasExists(facturas.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(facturas);
        }

        // GET: Facturas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var facturas = await _context.Facturas
                .SingleOrDefaultAsync(m => m.Id == id);
            if (facturas == null)
            {
                return NotFound();
            }

            return View(facturas);
        }

        // POST: Facturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var facturas = await _context.Facturas.SingleOrDefaultAsync(m => m.Id == id);
            _context.Facturas.Remove(facturas);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool FacturasExists(int id)
        {
            return _context.Facturas.Any(e => e.Id == id);
        }

        private List<SelectListItem> GetFincas()
        {
            List<SelectListItem> listaFincas = new List<SelectListItem>();

            var fincas = _context.Rodales.OrderBy(o => o.Finca).GroupBy(m => m.Finca).ToList();
            foreach( var finca in fincas)
            {
                listaFincas.Add(new SelectListItem() { Value = finca.Key, Text = finca.Key });
            }
            return listaFincas;
        }
    }
}
