﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EquiforestWebApp.Data;
using EquiforestWebApp.Models;

namespace EquiforestWebApp.Controllers
{
    public class PreciosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PreciosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Precios
        public async Task<IActionResult> Index()
        {
            return View(await _context.Precios.ToListAsync());
        }

        // GET: Precios/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var precios = await _context.Precios
                .SingleOrDefaultAsync(m => m.Id == id);
            if (precios == null)
            {
                return NotFound();
            }

            return View(precios);
        }

        // GET: Precios/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Precios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FacturaId,Clase,Estrato,PrecioA,PrecioB,PrecioC")] Precios precios)
        {
            if (ModelState.IsValid)
            {
                _context.Add(precios);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(precios);
        }

        // GET: Precios/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var precios = await _context.Precios.Where(m => m.FacturaId == id).ToListAsync();
            //SingleOrDefaultAsync(m => m.FacturaId == id);
            if (precios == null)
            {
                return NotFound();
            }
            return View(precios);
        }

        // POST: Precios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(List<Precios> precios)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    foreach(var precio in precios)
                    {
                        var p = _context.Precios.FirstOrDefault(m => m.Id == precio.Id);
                        if(p != null)
                        {
                            p.PrecioA = precio.PrecioA;
                            p.PrecioB = precio.PrecioB;
                            p.PrecioC = precio.PrecioC;
                        }
                    }
                    //_context.Precios.UpdateRange(precios);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction(nameof(Index),"Facturas");
            }
            return View(precios);
        }

        // GET: Precios/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var precios = await _context.Precios
                .SingleOrDefaultAsync(m => m.Id == id);
            if (precios == null)
            {
                return NotFound();
            }

            return View(precios);
        }

        // POST: Precios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var precios = await _context.Precios.SingleOrDefaultAsync(m => m.Id == id);
            _context.Precios.Remove(precios);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PreciosExists(int id)
        {
            return _context.Precios.Any(e => e.Id == id);
        }
    }
}
