﻿IF OBJECT_ID(N'__EFMigrationsHistory') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [AspNetRoles] (
    [Id] nvarchar(450) NOT NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    [Name] nvarchar(256) NULL,
    [NormalizedName] nvarchar(256) NULL,
    CONSTRAINT [PK_AspNetRoles] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [AspNetUsers] (
    [Id] nvarchar(450) NOT NULL,
    [AccessFailedCount] int NOT NULL,
    [ConcurrencyStamp] nvarchar(max) NULL,
    [Email] nvarchar(256) NULL,
    [EmailConfirmed] bit NOT NULL,
    [LockoutEnabled] bit NOT NULL,
    [LockoutEnd] datetimeoffset NULL,
    [NormalizedEmail] nvarchar(256) NULL,
    [NormalizedUserName] nvarchar(256) NULL,
    [PasswordHash] nvarchar(max) NULL,
    [PhoneNumber] nvarchar(max) NULL,
    [PhoneNumberConfirmed] bit NOT NULL,
    [SecurityStamp] nvarchar(max) NULL,
    [TwoFactorEnabled] bit NOT NULL,
    [UserName] nvarchar(256) NULL,
    CONSTRAINT [PK_AspNetUsers] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Facturas] (
    [Id] int NOT NULL IDENTITY,
    [BillOfLading] nvarchar(20) NULL,
    [Cliente] nvarchar(50) NULL,
    [Destino] nvarchar(20) NULL,
    [Especie] nvarchar(20) NULL,
    [FacturaId] nvarchar(10) NOT NULL,
    [Fecha] datetime2 NOT NULL,
    [Finca] nvarchar(20) NULL,
    [Incoterm] nvarchar(3) NULL,
    CONSTRAINT [PK_Facturas] PRIMARY KEY ([Id]),
    CONSTRAINT [AK_Facturas_FacturaId] UNIQUE ([FacturaId])
);

GO

CREATE TABLE [PackingList] (
    [Id] int NOT NULL IDENTITY,
    [Clase] nvarchar(max) NULL,
    [Consecutivo] int NOT NULL,
    [ContenedorId] varchar(20) NULL,
    [Estado] nvarchar(max) NULL,
    [Fecha] datetime2 NOT NULL,
    [PackingListId] varchar(30) NOT NULL,
    CONSTRAINT [PK_PackingList] PRIMARY KEY ([Id]),
    CONSTRAINT [AK_PackingList_PackingListId] UNIQUE ([PackingListId])
);

GO

CREATE TABLE [Rodales] (
    [Id] int NOT NULL IDENTITY,
    [Finca] nvarchar(max) NULL,
    [Nucleo] nvarchar(max) NULL,
    [Rodal] nvarchar(max) NULL,
    CONSTRAINT [PK_Rodales] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Viajes] (
    [Id] int NOT NULL IDENTITY,
    [FechaIngreso] datetime2 NOT NULL,
    [Finca] nvarchar(max) NOT NULL,
    [N_Viaje] int NOT NULL,
    [NoTrozasC] int NULL,
    [Nucleo] nvarchar(max) NOT NULL,
    [Rodal] nvarchar(max) NOT NULL,
    [Ubicacion] nvarchar(max) NOT NULL,
    [ViajeId] varchar(30) NOT NULL,
    CONSTRAINT [PK_Viajes] PRIMARY KEY ([Id]),
    CONSTRAINT [AK_Viajes_ViajeId] UNIQUE ([ViajeId])
);

GO

CREATE TABLE [AspNetRoleClaims] (
    [Id] int NOT NULL IDENTITY,
    [ClaimType] nvarchar(max) NULL,
    [ClaimValue] nvarchar(max) NULL,
    [RoleId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserClaims] (
    [Id] int NOT NULL IDENTITY,
    [ClaimType] nvarchar(max) NULL,
    [ClaimValue] nvarchar(max) NULL,
    [UserId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserLogins] (
    [LoginProvider] nvarchar(450) NOT NULL,
    [ProviderKey] nvarchar(450) NOT NULL,
    [ProviderDisplayName] nvarchar(max) NULL,
    [UserId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey]),
    CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserRoles] (
    [UserId] nvarchar(450) NOT NULL,
    [RoleId] nvarchar(450) NOT NULL,
    CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY ([UserId], [RoleId]),
    CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [AspNetRoles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [AspNetUserTokens] (
    [UserId] nvarchar(450) NOT NULL,
    [LoginProvider] nvarchar(450) NOT NULL,
    [Name] nvarchar(450) NOT NULL,
    [Value] nvarchar(max) NULL,
    CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY ([UserId], [LoginProvider], [Name]),
    CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [Contenedores] (
    [Id] int NOT NULL IDENTITY,
    [Cabezal] nvarchar(10) NULL,
    [Chofer] nvarchar(30) NULL,
    [ContenedorId] nvarchar(20) NOT NULL,
    [DescuentoCrc] real NOT NULL,
    [DescuentoLong] real NOT NULL,
    [FacturaId] nvarchar(10) NULL,
    [FechaSalida] datetime2 NOT NULL,
    [MaxCargoWgt] real NOT NULL,
    [PackingListId] nvarchar(30) NULL,
    [Sello] nvarchar(10) NULL,
    CONSTRAINT [PK_Contenedores] PRIMARY KEY ([Id]),
    CONSTRAINT [AK_Contenedores_ContenedorId] UNIQUE ([ContenedorId]),
    CONSTRAINT [AlternateKey_ContenedoresFactura] FOREIGN KEY ([FacturaId]) REFERENCES [Facturas] ([FacturaId]) ON DELETE NO ACTION
);

GO

CREATE TABLE [DetalleContenedores] (
    [Id] int NOT NULL IDENTITY,
    [Circunferencia] real NOT NULL,
    [Longitud] real NOT NULL,
    [PackingListId] varchar(30) NOT NULL,
    CONSTRAINT [PK_DetalleContenedores] PRIMARY KEY ([Id]),
    CONSTRAINT [AlternateKey_Contenedor] FOREIGN KEY ([PackingListId]) REFERENCES [PackingList] ([PackingListId]) ON DELETE CASCADE
);

GO

CREATE TABLE [Trozas] (
    [Id] int NOT NULL IDENTITY,
    [Circunferencia] real NULL,
    [Clasificacion] nvarchar(max) NULL,
    [Largo] real NULL,
    [Serial] int NOT NULL,
    [ViajeId] varchar(30) NULL,
    CONSTRAINT [PK_Trozas] PRIMARY KEY ([Id]),
    CONSTRAINT [AlternateKey_ViajeId] FOREIGN KEY ([ViajeId]) REFERENCES [Viajes] ([ViajeId]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_AspNetRoleClaims_RoleId] ON [AspNetRoleClaims] ([RoleId]);

GO

CREATE UNIQUE INDEX [RoleNameIndex] ON [AspNetRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL;

GO

CREATE INDEX [IX_AspNetUserClaims_UserId] ON [AspNetUserClaims] ([UserId]);

GO

CREATE INDEX [IX_AspNetUserLogins_UserId] ON [AspNetUserLogins] ([UserId]);

GO

CREATE INDEX [IX_AspNetUserRoles_RoleId] ON [AspNetUserRoles] ([RoleId]);

GO

CREATE INDEX [EmailIndex] ON [AspNetUsers] ([NormalizedEmail]);

GO

CREATE UNIQUE INDEX [UserNameIndex] ON [AspNetUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;

GO

CREATE INDEX [IX_Contenedores_FacturaId] ON [Contenedores] ([FacturaId]);

GO

CREATE INDEX [IX_DetalleContenedores_PackingListId] ON [DetalleContenedores] ([PackingListId]);

GO

CREATE INDEX [IX_Trozas_ViajeId] ON [Trozas] ([ViajeId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190805140626_Initial migration', N'2.0.3-rtm-10026');

GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'Trozas') AND [c].[name] = N'Serial');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Trozas] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Trozas] DROP COLUMN [Serial];

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190805163651_remove serial from trozas', N'2.0.3-rtm-10026');

GO

