﻿IF OBJECT_ID(N'__EFMigrationsHistory') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Precios] (
    [Id] int NOT NULL IDENTITY,
    [Clase] nvarchar(max) NULL,
    [Estrato] nvarchar(max) NULL,
    [FacturaId] nvarchar(10) NOT NULL,
    [PrecioA] real NOT NULL,
    [PrecioB] real NOT NULL,
    [PrecioC] real NOT NULL,
    CONSTRAINT [PK_Precios] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190820151812_add table precios', N'2.0.3-rtm-10026');

GO

