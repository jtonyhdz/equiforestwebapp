﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class Estratos
    {
        public int Id { get; set; }
        public float MinValue { get; set; }
        public float MaxValue { get; set; }
        public string Name { get; set; }
    }
}
