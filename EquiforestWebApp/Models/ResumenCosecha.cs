﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class ResumenCosecha
    {
        public float? Circunferencia { get; set; }
        public string Clasificacion { get; set; }
        public float? Largo { get; set; }
        public float? Volumen { get; set; }
        public int? NoTrozasC { get; set; }
        public float? VolTrozasC { get; set; }
    }
}
