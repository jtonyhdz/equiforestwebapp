﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace EquiforestWebApp.Models
{
    public class Facturas
    {
        public int Id { get; set; }
        [Required,MaxLength(10)]
        public string FacturaId { get; set; }
        [DataType(DataType.Date)]
        public DateTime Fecha { get; set; }
        [MaxLength(50)]
        public string Cliente { get; set; }
        [MaxLength(20)]
        public string Especie { get; set; }
        [MaxLength(20)]
        public string Finca { get; set; }
        [MaxLength(3)]
        public string Incoterm { get; set; }
        [MaxLength(20)]
        public string Destino { get; set; }
        [MaxLength(20)]
        public string BillOfLading { get; set; }
        public List<Contenedores> Contenedores { get; set; }
    }
}
