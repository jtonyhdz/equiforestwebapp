﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class ViajesGroup
    {
        //public int MyProperty { get; set; }
        public string Nucleo { get; set; }
        public string Finca { get; set; }
        public string Rodal { get; set; }
        public DateTime Fecha { get; set; }
        public string Ubicacion { get; set; }
        public int N_Viaje { get; set; }
        public string Clasificacion { get; set; }
        public float? Circunferencia { get; set; }
        public float? Longitud { get; set; }
        public float? Volumen { get; set; }
        public int? Cantidad { get; set; }
        public int? NoTrozasC { get; set; }
    }
}
