﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class Trozas
    {
        public int Id { get; set; }
        public string Clasificacion { get; set; }
        public float? Circunferencia { get; set; }
        public float? Largo { get; set; }
        [Column(TypeName = "varchar(30)")]
        public string ViajeId { get; set; }
        [ForeignKey("ViajeId")]
        public Viajes Viaje { get; set; }
    }
}
