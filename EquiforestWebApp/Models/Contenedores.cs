﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EquiforestWebApp.Models
{
    public class Contenedores
    {
        public int Id { get; set; }
        public DateTime FechaSalida { get; set; }
        [MaxLength(20)]
        public string ContenedorId { get; set; }
        [MaxLength(10)]
        public string Cabezal { get; set; }
        [MaxLength(10)]
        public string Sello { get; set; }
        [MaxLength(30)]
        public string Chofer { get; set; }
        public float DescuentoCrc { get; set; }
        public float DescuentoLong { get; set; }
        public float MaxCargoWgt { get; set; }
        [MaxLength(10)]
        public string FacturaId { get; set; }
        [MaxLength(30)]
        public string PackingListId { get; set; }
        [ForeignKey("FacturaId")]
        public Facturas Factura { get; set; }
        
    }
}
