﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace EquiforestWebApp.Migrations
{
    public partial class addtableprecios : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Precios",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Clase = table.Column<string>(nullable: true),
                    Estrato = table.Column<string>(nullable: true),
                    FacturaId = table.Column<string>(maxLength: 10, nullable: false),
                    PrecioA = table.Column<float>(nullable: false),
                    PrecioB = table.Column<float>(nullable: false),
                    PrecioC = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Precios", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Precios");
        }
    }
}
