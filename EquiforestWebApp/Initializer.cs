﻿using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace EquiforestWebApp
{
    public static class Initializer
    {
        public static async Task initial(RoleManager<IdentityRole> roleManager)
        {
            
            if (!await roleManager.RoleExistsAsync("Admin"))
            {
                var role = new IdentityRole("Admin");
                await roleManager.CreateAsync(role);
            } 

            if (!await roleManager.RoleExistsAsync("User"))
            {
                var role = new IdentityRole("User");
                await roleManager.CreateAsync(role);
            }

            if (!await roleManager.RoleExistsAsync("Harvest"))
            {
                var role = new IdentityRole("Harvest");
                await roleManager.CreateAsync(role);
            }

            if (!await roleManager.RoleExistsAsync("Sales"))
            {
                var role = new IdentityRole("Sales");
                await roleManager.CreateAsync(role);
            }

            if (!await roleManager.RoleExistsAsync("HarvestOperator"))
            {
                var role = new IdentityRole("HarvestOperator");
                await roleManager.CreateAsync(role);
            }

            if (!await roleManager.RoleExistsAsync("SalesOperator"))
            {
                var role = new IdentityRole("SalesOperator");
                await roleManager.CreateAsync(role);
            }


        }
    }
}